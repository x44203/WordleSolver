import wordle, random

#winning_word = "drink"#random.choice(wordle.words)

def check_wordle(word, winning_word):
    result = [""] * 5

    winning = []
    winning_letters = []
    maybe_letters = []

    for i, letter in enumerate(word):
        if word[i] == winning_word[i]:
            result[i] = "y"
            winning.append(i)
            winning_letters.append(word[i])
        
    for i, letter in enumerate(word):
        if i in winning:
            pass

        elif word[i] in winning_word and word[i] not in winning_letters and word[i] not in maybe_letters:
            result[i] += "o"
            maybe_letters.append(word[i])
        
        else:
            result[i] += "n"
    
    return "".join(result)


def iterate(winning_word):
    wordle.fixed = []
    wordle.variable = []
    wordle.forbidden = []

    word = "arose" #wordle.find_words(wordle.letters_by_occurrence[:wordle.word_length])[0] # arose

    for i in range(6):
        result = check_wordle(word, winning_word)

        if result == "yyyyy":
            return i + 1
        
        word = wordle.next_word(word, result, i)
    
    return 1000


tries = []

score = 0
count = 0
minscore = 1000
maxscore = 0

histogram = {}

words = len(wordle.words)

for word in wordle.words[0:words]:
    value = iterate(word)
    tries.append(value)
    
    if value == 1000:
        print(word)
        pass
    else:
        score += value
        maxscore = max(maxscore, value)
        minscore = min(minscore, value)
        histogram[value] = histogram.setdefault(value, 0) + 1
        count += 1

print("Score:", 7 - score / count)
print("Min:", minscore)
print("Max:", maxscore)
print("Histogram:")

for key in sorted(histogram.keys()):
    print(key, histogram[key])

print("Steps:", score / count)
print("Solved:", count / words * 100, "%")

print(min(tries))
print(max(tries))
#print(tries)