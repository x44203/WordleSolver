import dictionary

#print(len(words))
#print(len(words2))

word_length = 5
words = sorted(list(set(dictionary.words)))

#words = []

#with open("dictionary.csv") as dictfile:
#    for word in dictfile.readlines():
#        if len(word.strip()) == word_length:
#            #if len(set(word.strip())) == word_length:
#            words.append(word.strip())

#print(len(words))

available_words = []

alphabet = [chr(i + ord("a")) for i in range(26)]
bins = {letter: 0 for letter in alphabet}

for word in words:
    for char in word:
        bins[char] += 1

bins = {key: value for key, value in sorted(bins.items(), key = lambda item: -item[1])}


letters_by_occurrence = [pair for pair in bins]


def find_words(letters):
    result = []

    for word in words:
        truth = True

        for letter in letters:
            truth = truth and letter in word
        
        if truth:
            result.append(word)
    
    return result

#nl = letters_by_occurrence[15:19]
#for c in letters_by_occurrence[0:15]:
#    for i in range(5):
#        ab = nl.copy()
#        ab[i] = c
#        wodl = find_words(ab)
#        if len(wodl) > 0:
#            print(c, wodl)
#
#exit()

def find_wordle(fixed, variable, forbidden, penalize_variable = False):
    """
    fixed: [[letter, position], ...]
    variable: [[letter, not position], ...]
    forbidden: [letter, ...]
    """
    result = []

    for word in available_words:
        truth = True

        score = 3.0

        for letter, position in fixed:
            truth = truth and word[position] == letter

            if letter in forbidden:
                forbidden.remove(letter)

        if penalize_variable:
            for letter, position in variable:
                if letter in word:
                    score -= 0.5

        else:
            for letter, position in variable:
                truth = truth and word[position] != letter and letter in word

                if letter in forbidden:
                    forbidden.remove(letter)

        for letter in forbidden:
            truth = truth and letter not in word
        
        if truth:
            score = 1.0
            result.append([word, score])
    
    #print(fixed, variable, forbidden)
    #print(result)
    return result


def get_most_occurring_word(words, double_letter_factor = 0): # Factor of 0 means that no double letters are included
    intermediary = []

    for word in words:
        intermediary.append([word, sum([bins[letter] for letter in word[0]]) * word[1]])
    
    for i in range(len(intermediary)):
        #print(intermediary[i][0][0])
        #if intermediary[i][0][0] == "filly:":
        #    print(set(intermediary[i]))
        if len(set(intermediary[i][0][0])) < len(intermediary[i][0][0]):
            intermediary[i][1] *= double_letter_factor # Penalize double letters
    
    return sorted(intermediary, key = lambda x : -x[1])[0][0][0]


def next_word(word, result, step):
    #print(word, result, i)
    for position, value in enumerate(result):
        if value == "n":
            forbidden.append(word[position])

        if value == "o":
            variable.append([word[position], position])

        if value == "y":
            fixed.append([word[position], position])
            
    #if step <= 1:
    #    return get_most_occurring_word(find_wordle([], [], [letter for letter in word] + forbidden), 0)

    #if step == 0:
    #    return get_most_occurring_word(find_wordle([], [], [letter for letter in word]), 0)

    if step == 0:
        available_words.clear()
        #available_words.extend(sorted(list(set(dictionary.words))))
        available_words.extend(dictionary.words)

    else:
        available_words.remove(word)


        
    if step == 0:
        return "until"
    
    if step == 1:
        return "dumpy"
    
    #if step == 2:
    #    return "zymic"

        
    #return get_most_occurring_word(find_wordle(fixed, variable, forbidden), 1 - 0.5 / step)#0.63 + step * 0.07)
    #if step == 2:
    #    return get_most_occurring_word(find_wordle(fixed, variable, forbidden, True), 1.0 if step == 5 else 0.5)

    #print(find_wordle(fixed, variable, forbidden, False))
    return get_most_occurring_word(find_wordle(fixed, variable, forbidden, False), 1.0 if step == 5 else 0.4)
    #return find_wordle(fixed, variable, forbidden)[0]#-1 if step & 1 else 0]

    #wordles = find_wordle(fixed, variable, forbidden)
    #return wordles[min(len(wordles) - 1, 6 - step)]


fixed = []
variable = []
forbidden = []

word = "arose"#find_words(letters_by_occurrence[:word_length])[0] # arose

if __name__ == "__main__":
    for i in range(6):
        print(f"Try '{word}'")

        print("Type result, where n = no, did not occur in word, o = okay, did occur but at the wrong location, y = yes, did occur and at the right location (example: nnyno):")

        result = None
        while result == None:
            result = input("result: ")
            if len(result) != 5 or sum([letter not in ["n", "o", "y"] for letter in result]) > 0:
                print("The result should be five letters composed of n, o and y, for example: ynnon")
                result = None

        print()

        if result == "yyyyy":
            print("You won! Congratulations!")
            exit()

        word = next_word(word, result, i)

    print("You lost :(")