# WordleSolver

Solves [Wordle puzzles](https://www.powerlanguage.co.uk/wordle/)

Can solve 99 % of words!

Requirements: Python 3

How to use: `python worlde.py`

Example:

```
Try 'arose'
Type result, where n = no, did not occur in word, o = okay, did occur but at the wrong location, y = yes, did occur and at the right location (example: nnyno):
result: nynnn

Try 'print'
Type result, where n = no, did not occur in word, o = okay, did occur but at the wrong location, y = yes, did occur and at the right location (example: nnyno):
result: nyyyn

Try 'grind'
Type result, where n = no, did not occur in word, o = okay, did occur but at the wrong location, y = yes, did occur and at the right location (example: nnyno):
result: nyyyo

Try 'drink'
Type result, where n = no, did not occur in word, o = okay, did occur but at the wrong location, y = yes, did occur and at the right location (example: nnyno):
result: yyyyy

You won! Congratulations!
```